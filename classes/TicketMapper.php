<?php

class TicketMapper extends Mapper
{
    /**
     * Determine join we have to add to complete the Entity
     * @return String the join
     */
    public function join() {
        return "components c on (c.id = t.component_id)";
    }

    /**
     * Get all tickets
     * @return array of ticket entity
     */

    public function getTickets() {
        return $this->findAll('tickets', 'TicketEntity', $this->join());
    }

    /**
     * Get one ticket by its ID
     *
     * @param int $ticket_id The ID of the ticket
     * @return TicketEntity  The ticket
     */
    public function getTicketById($ticket_id) {
        return $this->findOneBy('tickets', 'TicketEntity', $this->join(), array('id' => $ticket_id));

    }


    public function save(TicketEntity $ticket) {
        $sql = "insert into tickets
            (title, description, component_id) values
            (:title, :description, 
            (select id from components where component = :component))";

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            "title" => $ticket->getTitle(),
            "description" => $ticket->getDescription(),
            "component" => $ticket->getComponent(),
        ]);

        if(!$result) {
            throw new Exception("could not save record");
        }
    }
}
