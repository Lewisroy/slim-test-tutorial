<?php

abstract class Mapper {
    protected $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function findAll($table, $class, $join) {
        $sql = "SELECT * FROM ".$table." t JOIN ".$join;
        $req = $this->db->query($sql);
        $results = [];
        while($row = $req->fetch()) {
            $results[] = new $class($row);
        }
        return $results;
    }

    public function findOneBy($table, $class, $join, array $champ, array $param = NULL) {
        $sql = "SELECT * FROM ".$table." t JOIN ".$join ;
        $where .= " where ";
        foreach($champ as $key => $value) {
            $where .= "t.".$key. " = :".$key;
        }
        $sql .= $where;
        

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute($champ);
        if($result) {
            return new TicketEntity($stmt->fetch());
        }
    }

    public function insert() {
        
    }

}
