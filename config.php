<?php
$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;
$config['db']['host']   = 'localhost';
$config['db']['user']   = 'root';
$config['db']['pass']   = 'root';
$config['db']['port']   = '8889';
$config['db']['unix_socket']   = '/Applications/MAMP/tmp/mysql/mysql.sock';
$config['db']['dbname'] = 'slim-test'; 

function init_container($container) {

    $container['view'] = new \Slim\Views\PhpRenderer("../templates/");

    $container['logger'] = function($c) {
        $logger = new \Monolog\Logger('my_logger');
        $file_handler = new \Monolog\Handler\StreamHandler('../logs/'.date('Y-m-d').'.log');
        $logger->pushHandler($file_handler);
        return $logger;
    };

    $container['db'] = function ($c) {
        $db = $c['settings']['db'];
        $pdo = new PDO('mysql:host=' . $db['host'] . ';port='.$db['host'].';dbname=' . $db['dbname'].';unix_socket='.$db['unix_socket'],
            $db['user'], $db['pass']);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    };

    return $container;
}